<?php

class Wp_Nrb_Begrepp {

    protected $loader;
    protected $plugin_name;
    protected $version;

    public function __construct($plugin_root) {
        $this->version = NRB_WP_BEGREPP_VERSION;
        $this->plugin_root = $plugin_root;

        $this->plugin_name = 'nrb_wp_begrepp';

        $this->load_dependencies();
        $this->define_admin_hooks();
        $this->define_public_hooks();
        $this->define_ajax_hooks();
    }

    public function attach_install() {
      register_activation_hook( $this->plugin_root, array($this, 'install') );
    }

    public function attach_uninstall() {
      register_deactivation_hook( $this->plugin_root, array($this, 'uninstall') );
    }

    public function install() {

        global $table_prefix, $wpdb;

        $tblname = $this->plugin_name.'_words';
        $wp_track_table = $table_prefix . "$tblname";

        // Check to see if the table exists already, if not, then create it
        if($wpdb->get_var( "show tables like '$wp_track_table'" ) != $wp_track_table)
        {
            update_option( $this->plugin_name.'_remote_source_uri' , 'http://storage.infopack.io/swe-nrb/nrb-begreppslista/latest/assets/concepts.json' );
            update_option( $this->plugin_name.'_remote_display_uri' , 'http://www.nationella-riktlinjer.se/concepts/' );
            update_option( $this->plugin_name.'_sync_warn_on_delete', false );
            update_option( $this->plugin_name.'_sync_remember_visibility_status', true );

            $sql = "
                CREATE TABLE `" . $wp_track_table . "`(
                    `id` INT NOT NULL AUTO_INCREMENT,
                    `uuid` CHAR(38) NOT NULL,
                    `slug` VARCHAR(255) NULL,
                    `concept_type` CHAR(1) NOT NULL,
                    `term` VARCHAR(255) NULL,
                    `msource` VARCHAR(255) NULL,
                    `definition` TEXT NULL,
                    `comment` TEXT NULL,
                    `is_visible` TINYINT NOT NULL DEFAULT '1',
                    `tags` TEXT NULL,
                    `synonyms` TEXT NULL,
                    PRIMARY KEY(`id`),
                    UNIQUE(`uuid`)
                ) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_general_ci COMMENT = 'stores definition data fetched from api';
            ";
            require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
            dbDelta($sql);
        }
    }

    public function uninstall() {
        global $table_prefix, $wpdb;

        $tblname = $this->plugin_name.'_words';
        $bk_tblname = $this->plugin_name.'_words_bk';
        $wp_track_table = $table_prefix . "$tblname";
        $wp_track_bk_table = $table_prefix . "$bk_tblname";

        $sql = "DROP TABLE IF EXISTS $wp_track_table;";
        $wpdb->query($sql);
        $sql = "DROP TABLE IF EXISTS $wp_track_bk_table;";
        $wpdb->query($sql);
        delete_option( $this->plugin_name.'_remote_source_uri' );
        delete_option( $this->plugin_name.'_remote_display_uri' );
        delete_option( $this->plugin_name.'_sync_warn_on_delete' );
        delete_option( $this->plugin_name.'_sync_remember_visibility_status' );
    }

    /**
     * Load the required dependecies for this plugin
     */
    private function load_dependencies() {
        /**
		 * The class is responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-nrb-wp-begrepp-loader.php';
        
        /**
         * The class is responsible for registration of ajax actions
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-nrb-wp-begrepp-ajax-actions.php';

        /**
        * The class responsible for defining all actions that occur in the admin area.
        */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-nrb-wp-begrepp-admin.php';
        
        /**
        * The class responsible for defining all actions that occur in the public area.
	    */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-nrb-wp-begrepp-public.php';

        $this->loader = new Nrb_Wp_Begrepp_Loader();
    }

    private function define_admin_hooks() {
      // admin actions
      /**
       * TODO: Admin hooks should be protected by is_admin()
       */
        $plugin = new Nrb_Wp_Begrepp_Admin( $this->get_plugin_name(), $this->get_version() );
        $plugin->define_hooks();
      
    }

    private function define_ajax_hooks() {
        $plugin = new Nrb_Wp_Begrepp_Ajax_Actions( $this->get_plugin_name(), $this->get_version() );
        $plugin->define_hooks();
    }
    
    private function define_public_hooks() {
        $plugin = new Nrb_Wp_Begrepp_Public( $this->get_plugin_name(), $this->get_version() );
        $this->loader->add_action( 'init', $plugin, 'add_shortcode' );
        $this->loader->add_action( 'init', $plugin, 'add_shortcode_tags' );
        $this->loader->add_action( 'init', $plugin, 'add_shortcode_metoder' );
        $this->loader->add_action( 'init', $plugin, 'add_shortcode_begrepp' );
        $this->loader->add_action( 'init', $plugin, 'taxonomy_comma_filter');
        $this->loader->add_action( 'wp_enqueue_scripts', $plugin, 'enqueue_assets' );
        
    }

    public function get_plugin_name() {
        return $this->plugin_name;
    }

    public function get_version() {
        return $this->version;
    }

    public function get_loader() {
        return $this->loader;
    }

    public function run() {
        $this->loader->run();
    }
}
