<?php

function get_property( $path, $object ) {
  return array_reduce(explode('.', $path), function ($o, $p) { return is_numeric($p) ? $o[$p] : $o->$p; }, $object);
}

class Nrb_Wp_Begrepp_Ajax_Actions {

  public function __construct( $plugin_name, $version ) {

    $this->plugin_name = $plugin_name;
    $this->version = $version;

  }

  public function define_hooks() {

    $this->add_apis();

  }

  public function add_apis() {

    add_action( 'wp_ajax_' . $this->plugin_name . '_fetch_options', array($this, 'ajax_fetch_options') );
    add_action( 'wp_ajax_' . $this->plugin_name . '_fetch_words', array($this, 'ajax_fetch_words') );
    add_action( 'wp_ajax_' . $this->plugin_name . '_create_backup', array($this, 'ajax_create_backup') );
    add_action( 'wp_ajax_' . $this->plugin_name . '_download_data', array($this, 'ajax_download_data') );
    add_action( 'wp_ajax_' . $this->plugin_name . '_merge_data', array($this, 'ajax_merge_data') );
    add_action( 'wp_ajax_' . $this->plugin_name . '_apply_flags', array($this, 'ajax_apply_flags') );

    add_action( 'wp_ajax_' . $this->plugin_name . '_add_custom_data', array($this, 'ajax_add_custom_data') );
    add_action( 'wp_ajax_' . $this->plugin_name . '_change_word_visibility', array($this, 'ajax_change_word_visibility') );
    add_action( 'wp_ajax_' . $this->plugin_name . '_edit_record', array($this, 'ajax_edit_record') );
    add_action( 'wp_ajax_' . $this->plugin_name . '_delete_record', array($this, 'ajax_delete_record') );
    add_action( 'wp_ajax_' . $this->plugin_name . '_taxonomy_import', array($this, 'ajax_taxonomy_import') );
  
  }

  public function ajax_fetch_options() {
      $ret = array();

      if ( isset($_GET) ) {
        global $wpdb;
        $options = !empty( $_GET['options']) ? $_GET['options']: '1';

        $data = array();
        foreach($options as $option) {
          $data[$option] = get_option( $this->plugin_name . '_' . $option );
        }

        $ret = array(
          'status' => 'OK',
          'data' => $data
        );

      } else {
        $ret = array(
          'status' => 'ERR',
          'message' => 'bad request'
        );
      }
      die( json_encode($ret) );
    }

    /**
     * Fetches all concepts from database, admin UI uses this to render table
     */
    public function ajax_fetch_words() {
      $ret = array();

      if ( isset($_GET) ) {
        global $wpdb;

        ob_start();
        $results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}{$this->plugin_name}_words WHERE 1 ORDER BY term ASC", ARRAY_A  );

        $ret = array(
          'status' => 'OK',
          'data'  => $results
        );

      } else {
        $ret = array(
          'status' => 'ERR',
          'message' => 'bad request'
        );
      }
      die( json_encode($ret) );
    }

    public function ajax_create_backup() {
      $ret = array();

      if ( isset($_POST) ) {
        global $wpdb;

        $wpdb->query("START TRANSACTION");
        $res1 = $wpdb->query("DROP TABLE IF EXISTS `{$wpdb->prefix}{$this->plugin_name}_words_bk`;");
        $res2 = $wpdb->query("CREATE TABLE `{$wpdb->prefix}{$this->plugin_name}_words_bk` LIKE `{$wpdb->prefix}{$this->plugin_name}_words`;");
        $res3 = $wpdb->query("INSERT `{$wpdb->prefix}{$this->plugin_name}_words_bk` SELECT * FROM `{$wpdb->prefix}{$this->plugin_name}_words`;");
        if($res1 && $res2 && $res3) {
          $res = $wpdb->query('COMMIT');
          if( $res ) {
            $ret = array(
              'status' => 'DONE',
            );
          } else {
            $ret = array(
              'status' => 'ERR',
              'message' => 'Error in creating backup (1)',
            );
          }
        } else {
          $wpdb->query('ROLLBACK');
          $ret = array(
            'status' => 'ERR',
            'message' => 'Error in creating backup (2)'
          );
        }

      } else {
        $ret = array(
          'status' => 'ERR',
          'message' => 'bad fat request'
        );
      }

      die(json_encode($ret));
    }

    public function ajax_download_data() {
      $ret = array();

      if ( isset($_POST) ) {
        global $wpdb;
        $error_records = array();
        $remote_source_uri = get_option( $this->plugin_name . '_Remote_Source_Uri' );
        if( empty($remote_source_uri) ) {
          status_header(500);
          die(json_encode(array(
            'status' => 'ERR',
            'message' => 'Bad plugin config'
          )));
        }
        $json_data = file_get_contents($remote_source_uri);
        if( empty($json_data) ) {
          status_header(500);
          die(json_encode(array(
            'status' => 'ERR',
            'message' => 'Infopack server return an empty response, is URL OK?'
          )));
        }

        $concepts = json_decode($json_data);
        if( empty($concepts) && !is_array($concepts) ) {
          status_header(500);
          die(json_encode(array(
            'status' => 'ERR',
            'message' => 'Could not parse payload from infopack server'
          )));
        }

        $insert_query_template = "
        INSERT INTO `{$wpdb->prefix}{$this->plugin_name}_words`(
          uuid,
          slug,
          concept_type,
          term,
          msource,
          definition,
          comment,
          is_visible,
          tags,
          synonyms
        )
        VALUES(%s, %s, 'I', %s, %s, %s, %s, 1, %s, %s);";

        $wpdb->query("START TRANSACTION");
        $wpdb->query("TRUNCATE TABLE `{$wpdb->prefix}{$this->plugin_name}_words`");
        reset($concepts);
        foreach($concepts as $concept) {

          try {
            $res = $wpdb->query( $wpdb->prepare($insert_query_template, array(
              $concept->id,
              $concept->slug,
              $concept->term,
              $concept->source,
              $concept->definition,
              $concept->comment,
              json_encode($concept->tags),
              json_encode($concept->synonyms)
            )));
          } catch(Exception $e) {
            $error_records[] = array(
              'id'   => $rec->id,
              'slug' => $rec->slug
            );
          }
        }
        if(count($error_records) > 0) {
          $wpdb->query("ROLLBACK");
          status_header(500);
          $ret = array(
            'status'        => 'ERR',
            'message'       => 'Could not store all records, action was rewinded',
            'error_count'   => count($error_records),
            'error_records' => $error_records
          );
        } else {
          $wpdb->query("COMMIT");
          $ret = array(
            'status'  => 'DONE',
            'message' => 'data read from remote source'
          );
        }
      } else {
        $ret = array(
          'status' => 'ERR',
          'message' => 'error in decoded data'
        );
      }
      die( json_encode($ret) );
    }

    public function ajax_merge_data() {
      $ret = array();

      if ( isset($_POST)) {
        global $wpdb;

        $query = "INSERT INTO `{$wpdb->prefix}{$this->plugin_name}_words` (SELECT * FROM `{$wpdb->prefix}{$this->plugin_name}_words_bk` WHERE concept_type='C');";

        $wpdb->query("START TRANSACTION");
        $res = $wpdb->query($query);
        if($res) {
          $wpdb->query("COMMIT");
          $ret = array(
            'status'  => 'DONE',
            'message' => 'data merged into main table'
          );
        } else {
          $wpdb->query("ROLLBACK");
          $ret = array(
            'status'        => 'FAIL',
            'message'       => 'data merge failed'
          );
        }
      } else {
        $ret=array(
          'status' => 'ERR',
          'message' => 'bad fat request'
        );
      }
      die( json_encode($ret) );
    }

    /**
     * Uses the backup table to restore visibility state
     */
    public function ajax_apply_flags() {
      $ret = array();

      if ( isset($_POST)) {
        global $wpdb;

        $query = "UPDATE `{$wpdb->prefix}{$this->plugin_name}_words` SET is_visible=0 WHERE uuid IN(SELECT uuid FROM `{$wpdb->prefix}{$this->plugin_name}_words_bk` WHERE concept_type='I' AND is_visible=0);";
        $res = $wpdb->query($query);

        $ret = array(
          'status'  => 'DONE',
          'message' => 'flags applied'
        );
      } else {
        $ret=array(
          'status' => 'ERR',
          'message' => 'bad fat request'
        );
      }
      die(json_encode($ret));
    }

    public function ajax_add_custom_data() {
      $ret = array();

      require NRB_WP_BEGREPP_ROOT.'inc/getUuid4.php';

      if ( isset($_POST) &&
          isset($_REQUEST) &&
          !empty($_REQUEST['term']) &&
          !empty($_REQUEST['msource']) &&
          !empty($_REQUEST['definition'])
        ) {
        global $wpdb;

        $uuid = strval(getUuid4());
        $term = $_REQUEST['term'];
        $msource = $_REQUEST['msource'];
        $definition = $_REQUEST['definition'];
        $comment = $_REQUEST['comment'];
        $is_visible = 1;

        $insert_query_template = "
          INSERT INTO `{$wpdb->prefix}{$this->plugin_name}_words`(
            uuid,
            concept_type,
            term,
            msource,
            definition,
            comment,
            is_visible,
            tags
          )
          VALUES(%s, 'C', %s, %s, %s, %s, %d, '[]');";

        $res = $wpdb->query($wpdb->prepare($insert_query_template, array($uuid, $term, $msource, $definition, $comment, $is_visible)));

        if($res) {
          $ret = array(
            'status' => 'DONE'
          );
        } else {
          $message = 'error adding custom data';
          $ret = array(
            'status' => 'ERR',
            'message' => $message
          );
        }
      } else {
        $message = 'bad fat request';
        $ret = array(
          'status' => 'ERR',
          'message' => $message
        );
      }
      die(json_encode($ret));
    }

    public function ajax_change_word_visibility() {
      if ( isset($_POST) &&
          isset($_REQUEST) &&
          isset($_REQUEST['id']) &&
          isset($_REQUEST['uuid']) &&
          isset($_REQUEST['is_visible'])
      ) {
        global $wpdb;
        $id = $_REQUEST['id'];
        $uuid = $_REQUEST['uuid'];
        $is_visible = ( ($_REQUEST['is_visible'] == 'true') || ($_REQUEST['is_visible'] == '1') || ($_REQUEST['is_visible'] == 1) ) ? 1 : 0;

        $update_query_template = "
          UPDATE `{$wpdb->prefix}{$this->plugin_name}_words`
          SET is_visible = %d
          WHERE id = %d AND uuid = %s;
        ";
        $res = $wpdb->query( $wpdb->prepare($update_query_template, array($is_visible, $id, $uuid)) );

        if($res) {
          $ret = array(
            'status' => 'DONE'
          );
        } else {
          $message = 'error changing visibility';
          $ret = array(
            'status' => 'ERR',
            'message' => $message
          );
        }
      } else {
        $message = 'bad fat request';
        $ret = array(
          'status' => 'ERR',
          'message' => $message
        );
      }
      die(json_encode($ret));
    }

    public function ajax_edit_record() {
      if ( isset($_POST) && isset($_REQUEST) &&
          isset($_REQUEST['id']) &&
          isset($_REQUEST['uuid']) &&
          isset($_REQUEST['term']) &&
          isset($_REQUEST['msource']) &&
          isset($_REQUEST['definition']) &&
          isset($_REQUEST['comment'])
      ) {
        global $wpdb;
        $id = $_REQUEST['id'];
        $uuid = $_REQUEST['uuid'];
        $term = $_REQUEST['term'];
        $msource = $_REQUEST['msource'];
        $definition = $_REQUEST['definition'];
        $comment = $_REQUEST['comment'];

        $update_query_template = "
        UPDATE `{$wpdb->prefix}{$this->plugin_name}_words`
        SET term=%s, msource=%s, definition=%s, comment=%s
        WHERE id = %d AND uuid = %s;";
        $res = $wpdb->query( $wpdb->prepare($update_query_template, array($term, $msource, $definition, $comment, $id, $uuid)) );
        if($res) {
          $ret = array(
            'status' => 'DONE'
          );
        } else {
          $message = 'error updating record';
          $ret = array(
            'status' => 'ERR',
            'message' => $message
          );
        }
      } else {
        $message = 'bad fat request';
        $ret=array(
          'status' => 'ERR',
          'message' => $message
        );
      }
      die( json_encode($ret) );
    }

    public function ajax_delete_record() {
      if ( isset($_POST) && isset($_REQUEST) &&
          isset($_REQUEST['id']) &&
          isset($_REQUEST['uuid'])
      ) {
        global $wpdb;
        $id = $_REQUEST['id'];
        $uuid = $_REQUEST['uuid'];

        $update_query_template = "
        DELETE FROM `{$wpdb->prefix}{$this->plugin_name}_words`
        WHERE id = %d AND uuid = %s;";
        $res = $wpdb->query( $wpdb->prepare($update_query_template, array($id, $uuid)) );
        if($res) {
          $ret = array(
            'status' => 'DONE'
          );
        } else {
          $message = 'error deleting record';
          $ret = array(
            'status' => 'ERR',
            'message' => $message
          );
        }
      } else {
        $message = 'bad fat request';
        $ret = array(
          'status' => 'ERR',
          'message' => $message
        );
      }
      die( json_encode($ret) );
    }

  public function ajax_taxonomy_import() {

    $remote_source_uri = get_option( $this->plugin_name . '_Remote_Source_Uri' );
    $data = json_decode( file_get_contents($remote_source_uri) );
    
    $to_insert = array();

    foreach ( $data as $item ) {

      $tmp = array(
        'term' => get_property('term', $item) . ' (' . get_property('source', $item) . ')',
        'slug' => get_property('slug', $item),
        'description' => get_property('definition', $item)
      );
      $to_insert[] = $tmp;

    }

    // results
    $error = array();
    $good = array();

    foreach ( $to_insert as $item ) {

        $db_result = wp_insert_term( $item['term'], 'begrepp', array('slug' => $item['slug'], 'description' => $item['description']) );
        if(is_wp_error($db_result)) {
          $error[] = $db_result;
        } else {
          $good[] = $db_result;
        }
    }
    
    die( json_encode(array('status' => 'ok', 'errors' => $error, 'result' => $good)) );
  }

} // end of class
