<?php

class Nrb_Wp_Begrepp_Public {

  private $plugin_name;
  private $version;

  public function __construct( $plugin_name, $version ) {
    $this->plugin_name = $plugin_name;
    $this->version = $version;
  }

  public function add_shortcode() {
    add_shortcode('nrb', array($this, 'shortcode') );
  }

  public function enqueue_assets() {
    wp_register_style( 'nrb-public-styles', ASSETS_PATH . 'css/public.css' );
    wp_enqueue_style( 'nrb-public-styles' );

    wp_register_script( 'nrb-public-scripts', ASSETS_PATH . 'js/public.js', null, null, true );
    wp_enqueue_script( 'nrb-public-scripts' );

  }

  public function shortcode($atts = [], $content = null) {

    $concepts = $this->get_tags_query();
    $content = '<table class="nrb-table"><thead><tr><th style="width:40%;">Begrepp</th><th>Förklaring</th></tr></thead><tbody>';
    if($concepts) {
      foreach ($concepts as $key => $concept) {
        if( !empty($concept['term']) && !empty($concept['msource']) && !empty($concept['definition']) && get_option('nrb_wp_begrepp_remote_display_uri') != '' ) {
          $content .= '<tr><td><a href="' . get_option('nrb_wp_begrepp_remote_display_uri') . $concept['slug'] . '">' . $concept['term'] . '</a> (' . $concept['msource'] . ')</td><td>' . $concept['definition'] . '</td></tr>';
        } else if( !empty($concept['term']) && !empty($concept['msource']) && !empty($concept['definition']) ) {
          $content .= '<tr><td><a>' . $concept['term'] . ' (' . $concept['msource'] . ')</td><td>' . $concept['definition'] . '</td></tr>';
        }
      }
    } else {
      $content .= '<tr><td colspan="2"><em>Det gick inte att ladda in begreppslistan</em></td></tr>';
    }

    $content .= '</tbody></table>';

    // always return
    return $content;
  }

  public function _comma_taxonomy_filter($tag_arr) {
    $custom_taxonomy_type = 'metoder';	// here goes your taxonomy type
    $tag_arr_new = $tag_arr;
    if($tag_arr->taxonomy == $custom_taxonomy_type && strpos($tag_arr->name, '--')) {
      $tag_arr_new->name = str_replace('--',',',$tag_arr->name);
    }
    return $tag_arr_new;	
  }

  public function _comma_taxonomies_filter($tags_arr) {
    $tags_arr_new = array();
    foreach($tags_arr as $tag_arr) {
      $tags_arr_new[] = $this->_comma_taxonomy_filter($tag_arr);
    }
    return $tags_arr_new;
  }

  public function taxonomy_comma_filter() {
    if(!is_admin()) {
      add_filter('get_the_taxonomies', array($this, '_comma_taxonomies_filter'));
      add_filter('get_terms', array($this, '_comma_taxonomies_filter'));
      add_filter('get_the_terms', array($this, '_comma_taxonomies_filter'));
    }
  }

  // new shortcode

  public function get_tags_query() {
    global $wpdb;

    $results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}{$this->plugin_name}_words WHERE is_visible=1 ORDER BY term ASC", ARRAY_A  );
    return ( $results ? $results : false );
  }
    

  public function add_shortcode_tags() {
    add_shortcode('nrb_begreppslista_cards', array($this, 'shortcode_tags') );
  }

  public function add_shortcode_metoder() {
    add_shortcode('nrb_metoder_tax', array($this, 'shortcode_metoder') );
  }
  public function add_shortcode_begrepp() {
    add_shortcode('nrb_begrepp_tax', array($this, 'shortcode_begrepp') );
  }

  public function shortcode_metoder() {

    $terms = get_the_terms( get_the_ID(), 'metoder' );
    if( $terms ){
      return $this->render_metoder_terms( $terms, 'metoder' );
    }
    
  }

  public function render_metoder_terms( $terms, $taxonomy ) {
    $html = '<ul class="list_' . $taxonomy . '">';
    
    foreach( $terms as $term ){
      $html .= '<li><a href="' . get_term_link( $term->term_id ) . '">' . $term->name . '</a></li>'; 
    }
    
    return $html .= '</ul>';
  
  }

  public function shortcode_begrepp() {

    $terms = get_the_terms( get_the_ID(), 'begrepp' );
    if( $terms ) {
      return $this->render_begrepp_terms( $terms, 'begrepp' );
    }
    
  }

  public function render_begrepp_terms( $terms, $taxonomy ) {
    $html = '<div class="list_' . $taxonomy . '">';
    
    foreach( $terms as $term ){
      $html .= '<span><a href="' . get_term_link( $term->term_id ) . '">' . $term->name . '</a></span> '; 
    }
    $html .= '</div>';
    // remove source from display
    $html = preg_replace( '/ \(.*?\)/m', "", $html);
    return $html;
  
  }

  public function shortcode_tags($atts = [], $content = null) {

    $concepts = $this->get_tags_query();

    $content = '';

    $content .= '
      <h2>Begreppslista</h2>
      <div class="nrb_search">
        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M19.745 17.2859L15.945 13.4859C16.8572 12.1112 17.3911 10.4651 17.3911 8.69556C17.3911 3.90083 13.4903 0 8.69556 0C3.90083 0 0 3.90083 0 8.69556C0 13.4903 3.90083 17.3911 8.69556 17.3911C10.4651 17.3911 12.1112 16.8572 13.4859 15.945L17.285 19.745C17.625 20.085 18.1755 20.085 18.5146 19.745L19.7441 18.5154C20.0841 18.1755 20.085 17.625 19.745 17.2859ZM1.73911 8.69556C1.73911 4.85995 4.85995 1.73911 8.69556 1.73911C12.5312 1.73911 15.652 4.85995 15.652 8.69556C15.652 12.5312 12.5312 15.652 8.69556 15.652C4.85995 15.652 1.73911 12.5312 1.73911 8.69556Z" fill="#1D5294"/></svg>
        <input name="nrb_search" id="tags_search" placeholder="Sök på begrepp" value="' . ( isset($_GET['tags_search']) ? $_GET['tags_search'] : '' ) . '"/>
        </div>
    </div>';

    if($concepts) {
      foreach ($concepts as $key => $concept) {

        if( empty($concept['term']) && empty($concept['msource']) && empty($concept['definition']) ) {
          continue;
        }
        $synonyms = $concept['synonyms'] === null ? array(): json_decode($concept['synonyms'], true);
        $content .= $this->render_tags_block( $concept['term'], $concept['definition'], $concept['comment'], $concept['msource'], json_decode($concept['tags'], true), $synonyms, $concept['slug'], $card_id );
        
      }
    } else {
      $content .= 'Det gick inte att ladda in begreppslistan';
    }

    return $content;
  }

  public function render_tags_block( $term_name, $definition, $comment, $msource, $tags, $synonyms, $slug, $card_id ) {
    
    $html_tags = '<div class="begrepp-tags-container">';
    foreach( $tags as $tag ) {
      $html_tags .= '<span class="tag" style="position: relative; padding: 10px 18px;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">' . $tag['short_name'] . '</font></font></span>';
    }
    $html_tags .= '</div>';
    $html_synonyms = '';
    if(isset($synonyms) && count($synonyms) > 0) {
      $html_synonyms .= '<div class="begrepp-synonyms-container">Se även: ';
      foreach( $synonyms as $synonym ) {
        $html_synonyms .= '<a href="?slug=' . $synonym['slug'] . '" class="synonym"><font style="vertical-align: inherit;">' . $synonym['name'] . '</font></font></a>';
      }
      $html_synonyms .= '</div>';
    }
    
    $html = '
      <div class="begrepp-wrapper js-tag-card" id="tag_' . $slug . '">
        <div class="arrow js-filter-card" data-filter-card="' . $slug . '"><i class="fas fa-arrow-right"></i></div> 
          <div class="begrepp-inner-wrapper">
            <h2><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">' . $term_name . '</font></font></h2>
            <h5><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">' .  $msource . '</font></font></h5>
            <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">' . $definition . '</font></p>
            <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">' . $comment . '</font></p>
            ' . $html_synonyms . $html_tags . '
          </div>
        </div>';

    return $html;
  }
 
}
