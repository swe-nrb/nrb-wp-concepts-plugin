<?php    // check user capabilities
  if ( ! current_user_can( 'manage_options' ) ) {
    return;
  }
?>

<div class="plugin-admin-wrap">
  <div>
    <h1 class="plugin-page-title"><?php echo esc_html( get_admin_page_title() ); ?></h1>
    <div class="plugin-page-subtitle" id="top_nav_root"></div>
  </div>
  <hr class="the-hr"/>
  <div class="progress-bar-container" id="plugin_progress_bar_root"></div>
  <div id="plugin_react_root"></div>
</div>
