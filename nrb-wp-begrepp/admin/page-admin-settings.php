<?php    // check user capabilities
    if ( ! current_user_can( 'manage_options' ) ) {
       return;
    }
?>
<div>
    <h3>Om</h3>
    <p>Tillgängliga shortcodes:</p>
    <ul>
        <li><strong>[nrb]</strong> - Begreppslista i tabellformat</li>
        <li><strong>[nrb_begreppslista_cards]</strong> - Begreppslista i kortformat</li>
        <li><strong>[nrb_metoder_tax]</strong> - Lista metodtaggar för en sida</li>
        <li><strong>[nrb_begrepp_tax]</strong> - Lista begreppstaggar för en sida</li>
    </ul>
</div>
<?php
    // add error/update messages

    // check if the user have submitted the settings
    // wordpress will add the "settings-updated" $_GET parameter to the url
    if ( isset( $_GET['settings-updated'] ) ) {
      // add settings saved message with the class of "updated"
      add_settings_error( $settingName, 'wporg_message', __( 'Settings saved', $this->plugin_name ), 'updated' );
    }

    // show error/update messages
    settings_errors($settingName);
?>
<div class="wrap">
    <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
    <form class="pure-form" action="options.php" method="post">
        <?php
            // output security fields for the registered setting "wporg"
            settings_fields( $settingName );
            // output setting sections and their fields
            // (sections are registered for "wporg", each field is registered to a specific section)
            do_settings_sections( $generalSettingsName );
            // output save settings button
        ?>
        <div>
            <button type="submit" class="button button-primary"><?php echo __('Save_Settings', $this->plugin_name); ?></button>
        </div>
    </form>
</div>
