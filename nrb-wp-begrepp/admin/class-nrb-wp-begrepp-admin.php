<?php

class Nrb_Wp_Begrepp_Admin {

  private $plugin_name;
  private $version;

  public function __construct( $plugin_name, $version ) {

    $this->plugin_name = $plugin_name;
    $this->version = $version;

  }

  public function define_hooks() {

    $this->register_pages();
    $this->register_taxonomies_and_fields();
    $this->add_settings();
    $this->add_scripts();

  }

  public function add_scripts() {
    add_action( 'admin_head', array($this, 'admin_head') );
    add_action( 'admin_enqueue_scripts', array($this, 'load_plugin_admin_script') );
  }

  public function admin_head() {

    // Load only on our plugin pages
    if(get_current_screen()->base != 'nrb-begreppslista_page_nrb_wp_begrepp_dashboard') {
      return;
    }
?>
<script type="text/javascript">
window.ASSETS_PATH = '<?php echo ASSETS_PATH;?>';
</script>
<?php
  }

  public function load_plugin_admin_script($hook) {
    // Load only on our plugin pages
    if($hook != 'nrb-begreppslista_page_nrb_wp_begrepp_dashboard') {
      return;
    }

    wp_enqueue_script('plugin-axios', 'https://unpkg.com/axios@0.18.0/dist/axios.js');
    wp_enqueue_script('plugin-babel-standalone', 'https://unpkg.com/babel-standalone@6/babel.min.js');
    wp_enqueue_script('plugin-react', 'https://unpkg.com/react@16/umd/react.production.min.js');
    wp_enqueue_script('plugin-react-dom', 'https://unpkg.com/react-dom@16/umd/react-dom.production.min.js');
    wp_enqueue_script('plugin-react-table', 'https://unpkg.com/react-table@7.0.0-alpha.14/react-table.min.js');
    wp_enqueue_script('plugin-react-router', 'https://cdnjs.cloudflare.com/ajax/libs/react-router/5.0.0/react-router.min.js');
    wp_enqueue_script('plugin-react-router-dom', 'https://cdnjs.cloudflare.com/ajax/libs/react-router-dom/5.0.0/react-router-dom.min.js');

    // pure css
    wp_enqueue_style( 'plugin-pure-css', 'https://unpkg.com/purecss@1.0.0/build/pure-min.css' );
    wp_enqueue_style('plugin-react-table', 'https://unpkg.com/react-table@7.0.0-alpha.14/react-table.css');
    
    // main style for plugin
    wp_register_style( 'plugin-admin-styles', ASSETS_PATH . 'css/main.css', array('plugin-pure-css') );
    wp_enqueue_style( 'plugin-admin-styles' );

    // jsx version
    wp_register_script( 'plugin-admin-react-jsx', ASSETS_PATH . 'jsx/main.jsx', null, null, true );
    wp_enqueue_script( 'plugin-admin-react-jsx' );

    // added to handle custom script type to support jsx. will be removed eventually after replacing jsx with js
    add_filter( 'script_loader_tag', array($this, 'filter_jsx_scripts'), 10, 3 );

  }

  public function filter_jsx_scripts( $tag, $handle, $src ) {
    if ( 'plugin-admin-react-jsx' === $handle ) {
      $tag = '<script type="text/babel" src="' . esc_url( $src ) . '" id="' . $handle . '" ></script>';
    }
    return $tag;
  }

  public function register_pages() {
    /**
     * register our admin pages to admin_menu
     */
    add_action( 'admin_menu', array($this, 'plugin_admin_dashboard') );
    add_action( 'admin_menu', array($this, 'plugin_setting_page') );
    add_action( 'admin_menu', array($this, 'plugin_taxonomy_import') );
  }

  public function register_taxonomies_and_fields() {
    add_action( 'init', array($this, 'sat_register_metoder_tax') );
    add_action( 'init', array($this, 'sat_register_begrepp_tax') );

    // Custom Fields
    add_action( 'metoder_add_form_fields' , array( $this, 'sat_add_metoder_reference_url' ) ); 
    add_action( 'metoder_edit_form_fields' , array( $this, 'sat_edit_metoder_reference_url' ) );
    add_action( 'create_metoder', array( $this, 'save_metoder_metadata' ) );
    add_action( 'edit_metoder', array( $this, 'save_metoder_metadata' ) );
    

    add_action( 'begrepp_add_form_fields', array( $this, 'sat_add_begrepp_reference_url' ) );
    add_action( 'begrepp_edit_form_fields' , array( $this, 'sat_edit_begrepp_reference_url' ) );
    add_action( 'create_begrepp', array( $this, 'save_begrepp_metadata' ) );

  }

  public function add_settings() {
    /**
     * register our plugin_settings_init to the admin_init action hook
     */
    add_action( 'admin_init', array($this, 'plugin_settings_init') );
  }

  /**
   * custom option and settings
   */
  public function plugin_settings_init() {

    $settingName = $this->plugin_name . '_settings';
    $generalSettingsName = $this->plugin_name . '_settings';
    $generalSettingsSectionName = $this->plugin_name . '_settings_section';

    register_setting( $settingName,
                      $this->plugin_name . '_remote_source_uri',
                      array(
                        'type'               => 'string',
                        'description'        => 'The url which the plugin use to fetch data (the source of truth)',
                        'sanitize_callback'  => array($this, 'sanitize_callback'),
                        'default'            => ''
                      )
    );

    register_setting( $settingName,
                      $this->plugin_name . '_remote_display_uri',
                      array(
                        'type'               => 'string',
                        'description'        => 'The url to public page',
                        'sanitize_callback'  => array($this, 'sanitize_callback'),
                        'default'            => ''
                      )
    );

    // register a new section in the "wporg" page
    add_settings_section(
      $generalSettingsSectionName,
      __( 'General_Settings_Section_Text', $this->plugin_name ),
      array($this, 'general_settings_render' ),
      $generalSettingsName
    );

    add_settings_field(
      $this->plugin_name.'_remote_source_uri',
      "Remote source URI",
      array($this, 'text_input_render' ),
      $generalSettingsName,
      $generalSettingsSectionName,
      [
        'label_for'         => $this->plugin_name . '_remote_source_uri',
        'class'             => 'form-group',
        'sub_class'         => 'regular-text code',
        'description'       => 'The url which the plugin use to fetch data (the source of truth)',
        'placeholder'       => ''
      ]
    );

    // register a new field
    add_settings_field(
      $this->plugin_name . '_remote_display_uri',
      "Remote display URI",
      array($this, 'text_input_render' ),
      $generalSettingsName,
      $generalSettingsSectionName,
      [
        'label_for'         => $this->plugin_name . '_remote_display_uri',
        'class'             => 'form-group',
        'sub_class'         => 'regular-text code',
        'description'       => 'The url to public page. If left blank the concepts will not be linked in UI.',
        'placeholder'       => ''
      ]
    );

  }

/**
 * these methods re used to sanitize the options
 */
public function sanitize_callback($input) {
  return $input;
}

/**
 * custom option and settings:
 * callback functions
 */

  public function general_settings_render( $args ) {
?>
<p id="<?php echo esc_attr( $args['id'] ); ?>"><?php esc_html_e( 'Enter your settings below:', $this->plugin_name ); ?>
</p>
<?php
  }

  public function text_input_render( $args ) {
    $option = get_option( $args['label_for'] );
?>
<input type="text" class="<?php echo esc_attr( $args['sub_class'] ); ?>"
    name="<?php echo esc_attr( $args['label_for'] ); ?>" id="<?php echo esc_attr( $args['label_for'] ); ?>"
    value="<?php echo esc_attr( $option ); ?>" placeholder="<?php echo esc_attr( $args['placeholder'] ); ?>" />
<p class="description">
    <?php esc_html_e( $args['description'], $this->plugin_name ); ?>
</p>
<?php
  }

  /**
  * top level menu
  */
  public function plugin_admin_dashboard() {

    // add top level menu page
    add_menu_page(
      'NRB Begreppslista',
      'NRB Begreppslista',
      'manage_options',
      $this->plugin_name,
      array($this, 'render_plugin_admin_dashboard'),
      null,
      66
    );
    
    $dashboardName = $this->plugin_name . '_dashboard';
    add_submenu_page(
      $this->plugin_name,
      'Dashboard',
      'Dashboard',
      'manage_options',
      $dashboardName,
      array($this, 'render_plugin_admin_dashboard')
    );

    remove_submenu_page($this->plugin_name, $this->plugin_name);
  }

  public function render_plugin_admin_dashboard() {
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/page-admin-dashboard.php';
  }

  public function plugin_setting_page() {
    $generalSettingsName = $this->plugin_name . '_settings';

    add_submenu_page(
      $this->plugin_name,
      'Settings',
      'Settings',
      'manage_options',
      $generalSettingsName,
      array($this, 'render_plugin_setting_page')
    );
    
  }

  public function render_plugin_setting_page() {
    $generalSettingsName = $this->plugin_name.'_settings';
    $settingName = $this->plugin_name.'_settings';
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/page-admin-settings.php';
  }

  public function plugin_taxonomy_import() {
    add_submenu_page(
      $this->plugin_name,
      'Taxonomy importer',
      'Taxonomy importer',
      'manage_options',
      $generalSettingsName = $this->plugin_name . '_taxonomy_importer',
      array($this, 'render_plugin_taxonomy_import')
    );
    
  }

  public function render_plugin_taxonomy_import() {
    $ajaxurl = admin_url('admin-ajax.php');
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/page-admin-taxonomy-import.php';
  }

  public function sat_register_metoder_tax() {
    $plural = _x( 'Metoder', 'taxonomy general name' );

    $singular = _x( 'Metoder', 'taxonomy singular name');
 
    $labels = array(
       'name'                       => $plural,
       'singular_name'              => $singular,
       'search_items'               => __( 'Search methods' ),
       'popularname_items'          => __( 'Popular methods' ),
       'all_items'                  => __( 'All methods' ),
       'parent_item'                => null,
       'parent_item_colon'          => null,
       'edit_item'                  => __( 'Edit method' ),
       'update_item'                => __( 'Update method' ),
       'add_new_item'               => __( 'Add New method' ),
       'new_item_name'              => __( 'New method' ),
       'separate_items_with_commas' => __( 'Separate methods with commas' ),
       'add_or_remove_items'        => __( 'Add or remove methods' ),
       'choose_from_most_used'      => __( 'Choose from the most used methods' ),
       'not_found'                  => __( 'No methods Found.' ),
       'menu_name'                  => __( 'Metoder' )
    );
 
    $args = array(
       'hierarchical'          => false,
       'labels'                => $labels,
       'show_ui'               => true,
       'show_admin_column'     => true,
       'update_count_callback' => __( '_update_post_term_count' ),
       'query_var'             => true,
       'rewrite'               => array( 'slug' => 'metoder' ),
       'show_in_rest'          => true,
    );
 
    register_taxonomy( 'metoder', array('page', 'post'), $args );
  }

  public function sat_register_begrepp_tax() {
     $plural = _x( 'Begrepp', 'taxonomy general name' );
  
     $singular = _x( 'Begrepp', 'taxonomy singular name');
  
     $labels = array(
        'name'                       => $plural,
        'singular_name'              => $singular,
        'search_items'               => __( 'Search Concept' ),
        'popularname_items'          => __( 'Popular Concept' ),
        'all_items'                  => __( 'All Concept' ),
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => __( 'Edit Concept' ),
        'update_item'                => __( 'Update Concept' ),
        'add_new_item'               => __( 'Add New Concept' ),
        'new_item_name'              => __( 'New Concept' ),
        'separate_items_with_commas' => __( 'Separate Concepts with commas' ),
        'add_or_remove_items'        => __( 'Add or remove Concepts' ),
        'choose_from_most_used'      => __( 'Choose from the most used Concepts' ),
        'not_found'                  => __( 'No Concepts Found.' ),
        'menu_name'                  => __( 'Begrepp' )
     );
  
     $args = array(
        'hierarchical'          => false,
        'labels'                => $labels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => __( '_update_post_term_count' ),
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'begrepp' ),
        'show_in_rest'          => true,
     );
  
     register_taxonomy( 'begrepp', array('page', 'post'), $args );
  }



 // add ref url term to metoder tax
 public function sat_add_metoder_reference_url(){
   wp_nonce_field( basename( __FILE__ ), 'sat_metoder_nonce' );
 ?>

<div class="form-field ref-url-metadata">

    <label for="metoder-reference-url-metadata">
        Reference URL
    </label>

    <input type="text" name="metoder_metoder-reference-url_metadata" id="metoder-reference-url-metadata" value=""
        class="ref-url-metadata-field" />
</div>
<?php
  
 }
 //-------------------------------------------------------------------------------------------
 public function sat_edit_metoder_reference_url( $term ) {
    wp_nonce_field( basename( __FILE__ ), 'sat_metoder_nonce' );
 
       $term_key = 'metoder_metoder-reference-url_metadata';
       $metadata = get_term_meta( $term ->term_id , $term_key, true);	  
 
  ?>
<tr class="form-field metoder-metadata">
    <th scope="row">
        <label for="metoder-reference-url-metadata">
            Reference URL
        </label>
    </th>

    <td>
        <input type="text" name="metoder_metoder-reference-url_metadata" id="metoder-reference-url-metadata"
            value="<?php echo( ! empty( $metadata ) ) ? esc_attr( $metadata) : ''; ?> " class=ref-url-metadata-field" />
    </td>
</tr>
<?php
    
  }
  
 public function save_metoder_metadata( $term_id ){
    
    if( !isset( $_POST[ 'sat_metoder_nonce' ] ) )
    {
      return;
    }
 
    if( ! wp_verify_nonce($_POST['sat_metoder_nonce'], basename( __FILE__ )))
    {
       return;
    }
 

       $term_key = $term_key = 'metoder_metoder-reference-url_metadata';
       
       if( isset( $_POST[ $term_key ] ) )
       {
          update_term_meta( $term_id, esc_attr( $term_key ), $_POST[ $term_key ]  );
       }
 }
  // add ref url term to begrepp tax
  public function sat_add_begrepp_reference_url()
  {
    wp_nonce_field( basename( __FILE__ ), 'sat_begrepp_nonce' );
  ?>

<div class="form-field ref-url-metadata">

    <label for="begrepp-reference-url-metadata">
        Reference URL
    </label>

    <input type="text" name="begrepp_begrepp-reference-url_metadata" id="begrepp-reference-url-metadata" value=""
        class="ref-url-metadata-field" />
</div>
<?php
    
    }

    public function sat_edit_begrepp_reference_url( $term ){

     wp_nonce_field( basename( __FILE__ ), 'sat_begrepp_nonce' );
  
        $term_key = 'begrepp_begrepp-reference-url_metadata';
        $metadata = get_term_meta( $term ->term_id , $term_key, true);	  
  
  ?>
<tr class="form-field begrepp-metadata">
    <th scope="row">
        <label for="begrepp-reference-url-metadata">
            Reference URL
        </label>
    </th>

    <td>
        <input type="text" name="begrepp_begrepp-reference-url_metadata" id="begrepp-reference-url-metadata"
            value="<?php echo( ! empty( $metadata ) ) ? esc_attr( $metadata) : ''; ?> " class=ref-url-metadata-field" />
    </td>
</tr>
<?php
        
  }
  
  
  public function save_begrepp_metadata( $term_id ){
      
      if( !isset( $_POST[ 'sat_begrepp_nonce' ] ) )
      {
        return;
      }

      if( ! wp_verify_nonce($_POST['sat_begrepp_nonce'], basename( __FILE__ )))
      {
        return;
      }


        $term_key = $term_key = 'begrepp_begrepp-reference-url_metadata';
        
        if( isset( $_POST[ $term_key ] ) )
        {
            update_term_meta( $term_id, esc_attr( $term_key ), $_POST[ $term_key ]  );
        }
  }

} // end of class
