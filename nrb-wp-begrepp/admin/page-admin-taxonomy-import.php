<?php    // check user capabilities
    if ( ! current_user_can( 'manage_options' ) ) {
       return;
    }
?>
<script>
    document.addEventListener('click', function (event) {
    if (!event.target.matches('#taxonomy-importer-btn')) return;
    document.getElementById("import-result-message").innerHTML = "Importerar... Lämna inte sidan...";
    event.preventDefault();
    fetch("<?php echo $ajaxurl ?>", {
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: "action=nrb_wp_begrepp_taxonomy_import"
    })
        .then((response) => response.json())
        .then(function(data) {
            console.log(data);
            document.getElementById("import-result-message").innerHTML = `Import klar. Antal fel ${data.errors.length}. Antal OK ${data.result.length}. Se konsolen för mer info.`;
        });

    }, false);
</script>
<div class="wrap">
    <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
    <h2>Begreppsimport</h2>
    <p>Begreppen hämtas från källan som är angiven under fliken settings.</p>
    <p><strong>OBS!</strong> Detta är en rak importfunktion som inte tar hänsyn till redan inlagda taxonomi termer.</p>
    <button id="taxonomy-importer-btn" class="primary primary-button">Starta import</button>
    <div id="import-result-message"></div>
</div>
