<?php
/*
Plugin name: Nationella riktlinjer BIM - Begreppslista
Description: Använd begreppslistan från Nationella riktlinjer BIM på din webbplats
Version: 2.2.3
Author: Lifehack AB
Author URI: https://www.lifehackab.se
License: MIT

Copyright (c) 2019 Lifehack AB

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

defined('NRB_WP_BEGREPP_VERSION') || define( 'NRB_WP_BEGREPP_VERSION', '2.2.3' );
defined('ASSETS_PATH') || define( 'ASSETS_PATH', plugins_url('assets/', __FILE__) );
defined('NRB_WP_BEGREPP_ROOT') || define( 'NRB_WP_BEGREPP_ROOT', __DIR__.'/' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-nrb-wp-begrepp.php';

function run_nrb_wp_begrepp() {
  $plugin_root = __FILE__;
  $plugin = new Wp_Nrb_Begrepp($plugin_root);
  $plugin->attach_install();
  $plugin->attach_uninstall();
  $plugin->run();
}

run_nrb_wp_begrepp();
