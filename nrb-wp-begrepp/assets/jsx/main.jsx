
(function(){
const { Fragment, useState, useEffect, useContext, useReducer } = React;
const { HashRouter, BrowserRouter, Route, Switch, IndexRoute, hashHistory, IndexLink, Link, browserHistory } = ReactRouterDOM;
const Router = HashRouter;
const ReactTable = window.ReactTable.default;
/**/

// point to containers' Node
const appRoot         = document.getElementById('plugin_react_root');
const topNavRoot      = document.getElementById('top_nav_root');
const progressBarRoot = document.getElementById('plugin_progress_bar_root');

/*!
 * AppContext
 **/
const AppContext = React.createContext({
  appRoot,
  topNavRoot,
  progressBarRoot
});
/**/

/*!
 * App Component
 **/
const App=function(){

  return (
    <div>
      <Router>
        <Switch>
          <Route exact path={'/'} component={Dashboard} />
          <Route path={'/sync-data'} component={SyncData} />
          <Route path={'/browse-data'} component={BrowseData} />
          <Route path={'/add-custom-data'} component={AddCustomData} />
        </Switch>
      </Router>
    </div>);
}/**/

/*!
 * Dashboard Component
 **/
const Dashboard = function(){

  return (<Fragment>
  <TopNav>
  :: <span>Dashboard</span>
  </TopNav>
  <div key={0} className="item-list">
    <div key={1} className="item sync-data">
      <Link to={'/sync-data'}><img className="icon" src={ASSETS_PATH+'icons/sync-data.svg'} alt="sync-data" /><div>Sync data</div></Link>
    </div>
    <div key={2} className="item browse-data">
      <Link to={'/browse-data'}><img className="icon" src={ASSETS_PATH+'icons/browse-data.svg'} alt="browse-data" /><div>Browse data</div></Link>
    </div>
    <div key={3} className="item add-custom-data">
      <Link to={'/add-custom-data'}><img className="icon" src={ASSETS_PATH+'icons/add-custom-data.svg'} alt="add-custom-data" /><div>Add custom data</div></Link>
    </div>
  </div>
  </Fragment>)
}/**/

/*
 * define action_names
 **/
  const action_names = {
    NOT_READY: 'NOT_READY',
    INIT: 'INIT',
    FETCH_OPTIONS: 'FETCH_OPTIONS',
    OPTIONS_FETCHED: 'OPTIONS_FETCHED',
    CHECKING_REMOTE_SOURCE_URI: 'CHECKING_REMOTE_SOURCE_URI',
    CREATING_BACKUP: 'CREATING_BACKUP',
    DOWNLOADING_DATA: 'DOWNLOADING_DATA',
    MERGING_DATA: 'MERGING_DATA',
    APPLYING_FLAGS: 'APPLYING_FLAGS',
    SYNC_FINISHED: 'SYNC_FINISHED',

    ERROR: 'ERROR',
    FETCH_OPTION_FAILED: 'FETCH_OPTION_FAILED',
    REMOTE_SOURCE_NOT_ACCESSIBLE: 'REMOTE_SOURCE_NOT_ACCESSIBLE',
    CREATE_BACKUP_FAILED: 'CREATE_BACKUP_FAILED',
    DOWNLOAD_DATA_FAILED: 'DOWNLOAD_DATA_FAILED',
    MERGE_DATA_FAILED: 'MERGE_DATA_FAILED',
    APPLY_FLAGS_FAILED: 'APPLY_FLAGS_FAILED'

  };/**/

/*
 * define view_components
 **/
  const view_components = {
    NOT_READY: (props)=>{
      return (<div>not ready! ({props.code})</div>);
    },
    LOADING: (props) => {
      return (<div>
        loading ...
      </div>);
    },
    SYNC_FORM: (props) => {
      const { remoteSourceUri, handleStartSync } = props;
      return (
      <div>
        <AlertMessage type="info"><b>Remote Source Uri:</b> <span>{remoteSourceUri}</span></AlertMessage>
        <br />
        <div>
          <button className="pure-button pure-button-primary" onClick={handleStartSync}>Start Sync</button>
        </div>
      </div>);
    },
    CHECKING_REMOTE_SOURCE_URI: (props) => {
      return (<div>
        <AlertMessage type="info"><b>Checking remote source URI</b> <span><img className="loading01" src={ASSETS_PATH + 'icons/loading01.gif'} /></span></AlertMessage>
      </div>);
    },
    CREATING_BACKUP: (props) => {
      return (<div>
        <AlertMessage type="success"><b>Checked remote source URI</b> <span><img className="done01" src={ASSETS_PATH + 'icons/checked.svg'} /></span></AlertMessage>
        <AlertMessage type="info"><b>Creating backup</b> <span><img className="loading01" src={ASSETS_PATH + 'icons/loading01.gif'} /></span></AlertMessage>
      </div>);

    },
    DOWNLOADING_DATA: (props) => {
      return (<div>
        <AlertMessage type="success"><b>Checked remote source URI</b> <span><img className="done01" src={ASSETS_PATH + 'icons/checked.svg'} /></span></AlertMessage>
        <AlertMessage type="success"><b>Backup created</b> <span><img className="done01" src={ASSETS_PATH + 'icons/checked.svg'} /></span></AlertMessage>
        <AlertMessage type="info"><b>Downloading data</b> <span><img className="loading01" src={ASSETS_PATH + 'icons/loading01.gif'} /></span></AlertMessage>
      </div>);
    },
    MERGING_DATA: (props) => {
      return (<div>
        <AlertMessage type="success"><b>Checked remote source URI</b> <span><img className="done01" src={ASSETS_PATH + 'icons/checked.svg'} /></span></AlertMessage>
        <AlertMessage type="success"><b>Backup created</b> <span><img className="done01" src={ASSETS_PATH + 'icons/checked.svg'} /></span></AlertMessage>
        <AlertMessage type="success"><b>Data Downloaded</b> <span><img className="done01" src={ASSETS_PATH + 'icons/checked.svg'} /></span></AlertMessage>
        <AlertMessage type="info"><b>Merging data</b> <span><img className="loading01" src={ASSETS_PATH + 'icons/loading01.gif'} /></span></AlertMessage>
      </div>);
    },
    APPLYING_FLAGS: (props) => {
      return (<div>
        <AlertMessage type="success"><b>Checked remote source URI</b> <span><img className="done01" src={ASSETS_PATH + 'icons/checked.svg'} /></span></AlertMessage>
        <AlertMessage type="success"><b>Backup created</b> <span><img className="done01" src={ASSETS_PATH + 'icons/checked.svg'} /></span></AlertMessage>
        <AlertMessage type="success"><b>Data Downloaded</b> <span><img className="done01" src={ASSETS_PATH + 'icons/checked.svg'} /></span></AlertMessage>
        <AlertMessage type="success"><b>Data Merged</b> <span><img className="done01" src={ASSETS_PATH + 'icons/checked.svg'} /></span></AlertMessage>
        <AlertMessage type="info"><b>Applying flags</b> <span><img className="loading01" src={ASSETS_PATH + 'icons/loading01.gif'} /></span></AlertMessage>
      </div>);
    },
    SYNC_FINISHED: (props) => {
      return (<div>
        <AlertMessage type="success"><b>Checked remote source URI</b> <span><img className="done01" src={ASSETS_PATH + 'icons/checked.svg'} /></span></AlertMessage>
        <AlertMessage type="success"><b>Backup created</b> <span><img className="done01" src={ASSETS_PATH + 'icons/checked.svg'} /></span></AlertMessage>
        <AlertMessage type="success"><b>Data Downloaded</b> <span><img className="done01" src={ASSETS_PATH + 'icons/checked.svg'} /></span></AlertMessage>
        <AlertMessage type="success"><b>Data Merged</b> <span><img className="done01" src={ASSETS_PATH + 'icons/checked.svg'} /></span></AlertMessage>
        <AlertMessage type="success"><b>Flags Applied</b> <span><img className="done01" src={ASSETS_PATH + 'icons/checked.svg'} /></span></AlertMessage>
        <AlertMessage type="info">
          <b>Sync finished! You may now start browsing data or add custom data</b>
          <span><img className="done01" src={ASSETS_PATH + 'icons/checked.svg'} /></span>
          {' '}<Link to={'/browse-data'}>browse data</Link>
          {' '}<Link to={'/add-custom-data'}>add custom data</Link>
        </AlertMessage>
      </div>);
    },
    ERROR: (props) => {
      const { code, message } = props;
      return (<div>
        <AlertMessage type="error"><b>Error (code: {code}) </b> - <span>{message}</span></AlertMessage>
      </div>);
    },

  };/**/

/*!
 * SyncData Component
 **/
const SyncData = function(){

/*
 * reducer_actions holds array of functions called based on actions happen
 * in component and updates the state of component
*/
const reducer_actions = {};

reducer_actions[action_names.NOT_READY] = (curState, action) => {
  return {...curState,
    stateName: action.type,
    ViewComponent: view_components.NOT_READY,
    viewProps: {
      code: action.payload.code
    }
  };
};

reducer_actions [action_names.INIT] = (curState, action)=>{
  return {
    ...curState,
    stateName: action.type
  };
};

reducer_actions [action_names.FETCH_OPTIONS] = (curState, action)=>{
  return {
    ...curState,
    stateName: action.type
  };
};

reducer_actions [action_names.OPTIONS_FETCHED] = (curState, action)=>{
  const {ViewComponent, viewProps, viewChildren, options} = action.payload;
  return {
    ...curState,
    stateName: action.type,
    ViewComponent,
    viewProps: {
      ...viewProps,
      remoteSourceUri: options.remoteSourceUri
    },
    viewChildren,
    options: options
  };
};

reducer_actions [action_names.CHECKING_REMOTE_SOURCE_URI] = (curState, action)=>{
  const {ViewComponent, viewProps, viewChildren, options} = action.payload;
  return {
    ...curState,
    stateName: action.type,
    ViewComponent,
    viewProps: {
      ...viewProps
    },
    viewChildren,
    options: options
  };
};

reducer_actions [action_names.CREATING_BACKUP] = (curState, action)=>{
  const {ViewComponent, viewProps, viewChildren, options} = action.payload;
  return {
    ...curState,
    stateName: action.type,
    ViewComponent,
    viewProps: {
      ...viewProps
    },
    viewChildren,
    options: options
  };
};

reducer_actions [action_names.DOWNLOADING_DATA] = (curState, action)=>{
  const {ViewComponent, viewProps, viewChildren, options} = action.payload;
  return {
    ...curState,
    stateName: action.type,
    ViewComponent,
    viewProps: {
      ...viewProps
    },
    viewChildren,
    options: options
  };
};

reducer_actions [action_names.MERGING_DATA] = (curState, action)=>{
  const {ViewComponent, viewProps, viewChildren, options} = action.payload;
  return {
    ...curState,
    stateName: action.type,
    ViewComponent,
    viewProps: {
      ...viewProps
    },
    viewChildren,
    options: options
  };
};

reducer_actions [action_names.APPLYING_FLAGS] = (curState, action)=>{
  const {ViewComponent, viewProps, viewChildren, options} = action.payload;
  return {
    ...curState,
    stateName: action.type,
    ViewComponent,
    viewProps: {
      ...viewProps
    },
    viewChildren,
    options: options
  };
};

reducer_actions [action_names.SYNC_FINISHED] = (curState, action)=>{
  const {ViewComponent, viewProps, viewChildren, options} = action.payload;
  return {
    ...curState,
    stateName: action.type,
    ViewComponent,
    viewProps: {
      ...viewProps
    },
    viewChildren,
    options: options
  };
};

reducer_actions [action_names.ERROR] = (curState, action)=>{
  const {ViewComponent, viewProps, viewChildren, options} = action.payload;
  return {
    ...curState,
    stateName: action.type,
    ViewComponent,
    viewProps: {
      ...viewProps
    },
    viewChildren,
    options: options
  };
};

/*
 * runs a reducer action based on action.type
*/
  const reducer = (curState, action) => {
    if(reducer_actions && action && action.type && reducer_actions[action.type] && (typeof reducer_actions[action.type] === "function") ){
      return reducer_actions[action.type](curState, action);
    }else{
      console.log('error running reducer action',reducer_actions,action);
    }
  }

  // initial values for ViewComponent and viewProps and viewChildren (dynamic view) which are gonna be changed based on component state
  let initialViewComponent= view_components.LOADING;
  let initialViewProps = null;
  let initialViewChildren = null;

  // initial state of component
  const initialState = {
    stateName: action_names.INIT,
    ViewComponent: initialViewComponent,
    viewProps: initialViewProps,
    viewChildren: initialViewChildren
  };

  // used by axios to generate cancel request handlers
  const CancelToken = axios.CancelToken;

  // react hook (useReducer)
  const [state, dispatch] = useReducer( reducer, initialState );

  // react hook (useEffect) listens to state change and run an action based on state
  useEffect(()=>{
    if(actions && state && state.stateName && actions[state.stateName] && (typeof actions[state.stateName] === "function") ){
      return actions[state.stateName]();
    }else{
      console.log('error running action',actions,state);
    }
  }, [state]);

  // runs after component mounted. so we can start using component do ajax calls etc
  useEffect(()=>{
    actions.INIT();
  }, []);

  // actions array which is array of functions that are gonna be called based on changed state
  const actions = {
    INIT: ()=>{
      // this cause a state change
      dispatch({
        // type holds the name of state we want to go in
        type: action_names.FETCH_OPTIONS
      });
    },
    NOT_READY: ()=>{

    },
    FETCH_OPTIONS: ()=>{
      let cancel;
      // send ajax request to fetch current remote source uri
      axios.get(ajaxurl, {
        params: {
          action: 'nrb_wp_begrepp_fetch_options',
          options: ['Remote_Source_Uri']
        },
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          cancel = c;
        })
      })
      .then(function (response) {

        if(response && response.data && response.data.status){
          const res = response.data;
          if(res.status === 'OK'){
            if(res.data && res.data.Remote_Source_Uri){

              dispatch({
                type: action_names.OPTIONS_FETCHED,
                // holds extra parameters send to reducer function when changing state (used to help reducer sets the new state)
                payload:{
                  //dynamic component component
                  ViewComponent: view_components.SYNC_FORM,
                  //dynamic component props
                  viewProps: {
                    handleStartSync: (e)=>{
                      dispatch({
                        type: action_names.CHECKING_REMOTE_SOURCE_URI,
                        payload:{
                          ViewComponent: view_components.CHECKING_REMOTE_SOURCE_URI,
                          options: {
                            remoteSourceUri: res.data.Remote_Source_Uri
                          }
                        }
                      });
                    }
                  },
                  // holds loaded options from server for later access
                  options: {
                    remoteSourceUri: res.data.Remote_Source_Uri
                  }
                }
              });
            }else{
              dispatch({
                // this sets the state of component to error and specifies the error code and message
                type: action_names.ERROR,
                payload: {
                  ViewComponent: view_components.ERROR,
                  viewProps:{
                    message: 'error fetching options',
                    code:1
                  }
                }
              });
            }
          }else if(res.message){
            dispatch({
              type: action_names.ERROR,
              payload: {
                ViewComponent: view_components.ERROR,
                viewProps:{
                  message: res.message,
                  code:2
                }
              }
            });
          }else{
            dispatch({
              type: action_names.ERROR,
              payload: {
                ViewComponent: view_components.ERROR,
                viewProps:{
                  message: 'Unknown error',
                  code:3
                }
              }
            });
          }
        }else{
          dispatch({
            type: action_names.ERROR,
            payload: {
              ViewComponent: view_components.ERROR,
              viewProps:{
                message: 'Unknown error',
                code:4
              }
            }
          });
        }
      })
      .catch(function (error) {
        if (axios.isCancel(error)) {
          dispatch({
            type: action_names.ERROR,
            payload: {
              ViewComponent: view_components.ERROR,
              viewProps:{
                message: 'Request canceled' + error.message,
                code:5
              }
            }
          });
        } else {
          dispatch({
            type: action_names.ERROR,
            payload: {
              ViewComponent: view_components.ERROR,
              viewProps:{
                message: 'error fetching Remote_Source_Uri',
                code:6
              }
            }
          });
        }
      })
      .then(function (){
        cancel();
      })

    },
    OPTIONS_FETCHED: ()=>{
    },
    CHECKING_REMOTE_SOURCE_URI: ()=>{
      let cancel;
      axios.get(state.options.remoteSourceUri, {

        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          cancel = c;
        })
      })
      .then(function (response) {
        if(response && response.status == 200 ){
          dispatch({
            type: action_names.CREATING_BACKUP,
            payload: {
              ViewComponent: view_components.CREATING_BACKUP
            }
          })
        }else{
          dispatch({
            type: action_names.ERROR,
            payload: {
              ViewComponent: view_components.ERROR,
              viewProps:{
                message: 'error checking remote source uri',
                code:7
              }
            }
          });
        }
      })
      .catch(function (error) {
        if (axios.isCancel(error)) {
          dispatch({
            type: action_names.ERROR,
            payload: {
              ViewComponent: view_components.ERROR,
              viewProps:{
                message: 'request canceled: ' + error.message ,
                code:8
              }
            }
          });
        } else {
          dispatch({
            type: action_names.ERROR,
            payload: {
              ViewComponent: view_components.ERROR,
              viewProps:{
                message: 'Unknown error',
                code:9
              }
            }
          });
        }
      })
      .then(function () {
//        cancel();
      })


    },
    CREATING_BACKUP: ()=>{
      let cancel;
      axios.request({
        url: ajaxurl,
        method: 'post',
        params: {
          action: 'nrb_wp_begrepp_create_backup',
        },
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          cancel = c;
        })
      })
      .then(function (response) {
        if(response && response.status == 200 ){
          dispatch({
            type: action_names.DOWNLOADING_DATA,
            payload: {
              ViewComponent: view_components.DOWNLOADING_DATA
            }
          })
        }else{
          dispatch({
            type: action_names.ERROR,
            payload: {
              ViewComponent: view_components.ERROR,
              viewProps:{
                message: 'error creating backup',
                code:10
              }
            }
          });
        }
      })
      .catch(function (error) {
        if (axios.isCancel(error)) {
          dispatch({
            type: action_names.ERROR,
            payload: {
              ViewComponent: view_components.ERROR,
              viewProps:{
                message: 'request canceled: '+error.message,
                code:11
              }
            }
          });
        } else {
          dispatch({
            type: action_names.ERROR,
            payload: {
              ViewComponent: view_components.ERROR,
              viewProps:{
                message: 'Unknown error',
                code:12
              }
            }
          });
        }
      })
      .then(function () {})

    },
    DOWNLOADING_DATA: () => {
      let cancel;
      axios.request({
        url: ajaxurl,
        method: 'post',
        params: {
          action: 'nrb_wp_begrepp_download_data',
        },
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          cancel = c;
        })
      })
      .then(function (response) {
        if(response && response.status == 200 ){
          dispatch({
            type: action_names.MERGING_DATA,
            payload: {
              ViewComponent: view_components.MERGING_DATA
            }
          })
        }else{
          dispatch({
            type: action_names.ERROR,
            payload: {
              ViewComponent: view_components.ERROR,
              viewProps:{
                message: 'error downloading data',
                code:13
              }
            }
          });
        }
      })
      .catch(function (error) {
        if (axios.isCancel(error)) {
          dispatch({
            type: action_names.ERROR,
            payload: {
              ViewComponent: view_components.ERROR,
              viewProps:{
                message: 'request canceled: '+error.message,
                code:14
              }
            }
          });
        } else {
          dispatch({
            type: action_names.ERROR,
            payload: {
              ViewComponent: view_components.ERROR,
              viewProps:{
                message: 'Unknown error',
                code:15
              }
            }
          });
        }
      })
      .then(function () {})
    },
    MERGING_DATA: () => {
      let cancel;
      axios.request({
        url: ajaxurl,
        method: 'post',
        params: {
          action: 'nrb_wp_begrepp_merge_data',
        },
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          cancel = c;
        })
      })
      .then(function (response) {
        if(response && response.status == 200 ){
          dispatch({
            type: action_names.APPLYING_FLAGS,
            payload: {
              ViewComponent: view_components.APPLYING_FLAGS
            }
          })
        }else{
          dispatch({
            type: action_names.ERROR,
            payload: {
              ViewComponent: view_components.ERROR,
              viewProps:{
                message: 'error merging data',
                code:16
              }
            }
          });
        }
      })
      .catch(function (error) {
        if (axios.isCancel(error)) {
          dispatch({
            type: action_names.ERROR,
            payload: {
              ViewComponent: view_components.ERROR,
              viewProps:{
                message: 'request canceled: ' + error.message,
                code:17
              }
            }
          });
        } else {
          dispatch({
            type: action_names.ERROR,
            payload: {
              ViewComponent: view_components.ERROR,
              viewProps:{
                message: 'Unknown error',
                code:18
              }
            }
          });
        }
      })
      .then(function () {})

    },
    APPLYING_FLAGS: () => {
      let cancel;
      axios.request({
        url: ajaxurl,
        method: 'post',
        params: {
          action: 'nrb_wp_begrepp_apply_flags',
        },
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          cancel = c;
        })
      })
      .then(function (response) {
        if(response && response.status == 200 ){
          dispatch({
            type: action_names.SYNC_FINISHED,
            payload: {
              ViewComponent: view_components.SYNC_FINISHED
            }
          })
        }else{
          dispatch({
            type: action_names.ERROR,
            payload: {
              ViewComponent: view_components.ERROR,
              viewProps:{
                message: 'error applying flags',
                code:19
              }
            }
          });
        }
      })
      .catch(function (error) {
        if (axios.isCancel(error)) {
          dispatch({
            type: action_names.ERROR,
            payload: {
              ViewComponent: view_components.ERROR,
              viewProps:{
                message: 'request canceled: '+error.message,
                code:20
              }
            }
          });
        } else {
          dispatch({
            type: action_names.ERROR,
            payload: {
              ViewComponent: view_components.ERROR,
              viewProps:{
                message: 'Unknown error',
                code:21
              }
            }
          });
        }
      })
      .then(function () {})

    },
    SYNC_FINISHED: ()=>{
    },

    ERROR: ()=>{
    },

    FETCH_OPTION_FAILED: 'FETCH_OPTION_FAILED',
    REMOTE_SOURCE_NOT_ACCESSIBLE: 'REMOTE_SOURCE_NOT_ACCESSIBLE',
    CREATE_BACKUP_FAILED: 'CREATE_BACKUP_FAILED',
    DOWNLOAD_DATA_FAILED: 'DOWNLOAD_DATA_FAILED',
    MERGE_DATA_FAILED: 'MERGE_DATA_FAILED',
    APPLY_FLAGS_FAILED: 'APPLY_FLAGS_FAILED'
  };

  const { ViewComponent, viewProps, viewChildren } = state;

  return (<Fragment>
  <TopNav>
  :: <Link to={'/'}>Dashboard</Link> > <span>sync-data</span>
  </TopNav>
  <ViewComponent {...viewProps}>{viewChildren}</ViewComponent>
  </Fragment>)
}/**/

class BrowseData extends React.Component{
  constructor (props){
    super(props)
    this.cancel=null;
    this.cancel2=null;
    this.cancel3=null;
    this.cancel4=null;
    this.cancel5=null;

    this.renderTermCell = this.renderTermCell.bind(this)
    this.renderMsourceCell = this.renderMsourceCell.bind(this)
    this.renderDefinitionCell = this.renderDefinitionCell.bind(this)
    this.renderTagsCell = this.renderTagsCell.bind(this)
    this.renderOperationsCell = this.renderOperationsCell.bind(this)

    this.handleRecordSaveEdit = this.handleRecordSaveEdit.bind(this);
    this.handleRecordCancelEdit = this.handleRecordCancelEdit.bind(this);
    this.handleHideRecord = this.handleHideRecord.bind(this);
    this.handleShowRecord = this.handleShowRecord.bind(this);
    this.handleEditRecordClick = this.handleEditRecordClick.bind(this);
    this.handleDeleteRecordClick = this.handleDeleteRecordClick.bind(this);
    this.handleSelectedRecordTermChange = this.handleSelectedRecordTermChange.bind(this);
    this.handleSelectedRecordMsourceChange = this.handleSelectedRecordMsourceChange.bind(this);
    this.handleSelectedRecordDefinitionChange = this.handleSelectedRecordDefinitionChange.bind(this);

    this.changeRecordVisibilityStatus = this.changeRecordVisibilityStatus.bind(this);
    this.init();
  }

  init() {
    const columns = [
      {
        Header: 'Term',
        accessor: 'term', // String-based value accessors!
        Cell: this.renderTermCell
      }, {
        Header: 'Source',
        accessor: 'msource',
        Cell: this.renderMsourceCell
      }, {
        Header: 'Definition',
        accessor: 'definition',
        Cell: this.renderDefinitionCell
      }, {
        Header: 'Tags',//props => <span>Friend Age</span>, // Custom header components!
        accessor: 'tags',
        Cell: this.renderTagsCell
      }, {
        id: 'operationsCol',
        Header: 'Operations',// Custom header component
        Cell: this.renderOperationsCell
      }
    ];

    const getTrProps = (state, rowInfo, column) => {
      if(rowInfo){
        return {
          style: {
            background: rowInfo.original.is_visible == '1'? '#fff' : '#eee',
            color: rowInfo.original.is_visible == '1'? '#333' : '#666'
          }
        };
      }else{
        return {};
      }
    }

    const tableProps = {
      data: [],
      columns,
      getTrProps
    };

    this.state={
      requestProgress: 0,
      requestStatus: PROGRESS_STATUS.NOT_STARTED,
      tableData: null,
      tableProps: tableProps,
      selectedRecord: null,
      content: (<span>Loading ... </span>)
    };
  }

  handleSelectedRecordTermChange(e){
    let selectedRecord = this.state.selectedRecord;
    selectedRecord.original.term = e.target.value;
    this.setState({
      selectedRecord
    }, ()=>{
      this.setState({
        content: this.renderTable()
      })
    })
  }

  renderTermCell(props){
    const { selectedRecord } = this.state;


    if(selectedRecord !== undefined && selectedRecord !== null && props.index == selectedRecord.index){
      return (<input type="text" className="pure-input-1" value={selectedRecord.original.term} onChange={this.handleSelectedRecordTermChange} />);
    }else{
      return (<span>{props.value}</span>);
    }
  }

  handleSelectedRecordMsourceChange(e){
    let selectedRecord = this.state.selectedRecord;
    selectedRecord.original.msource = e.target.value;
    this.setState({
      selectedRecord
    }, ()=>{
      this.setState({
        content: this.renderTable()
      })
    })
  }

  renderMsourceCell(props){
    const { selectedRecord } = this.state;


    if(selectedRecord !== undefined && selectedRecord !== null && props.index == selectedRecord.index){
      return (<input type="text" className="pure-input-1" value={selectedRecord.original.msource} onChange={this.handleSelectedRecordMsourceChange} />);
    }else{
      return (<span>{props.value}</span>);
    }
  }

  handleSelectedRecordDefinitionChange(e){
    let selectedRecord = this.state.selectedRecord;
    selectedRecord.original.definition = e.target.value;
    this.setState({
      selectedRecord
    }, ()=>{
      this.setState({
        content: this.renderTable()
      })
    })
  }

  renderDefinitionCell(props){
    const { selectedRecord } = this.state;


    if(selectedRecord !== undefined && selectedRecord !== null && props.index == selectedRecord.index){
      return (<textarea rows="2" type="text" className="pure-input-1" value={selectedRecord.original.definition} onChange={this.handleSelectedRecordDefinitionChange} ></textarea>);
    }else{
      return (<span>{props.value}</span>);
    }
  }

  renderTagsCell(props){
    const tmpData = JSON.parse(props.value);
    let tmpRet = [];
    let i=0;
    tmpData.forEach((el)=>{
      tmpRet.push(<span key={i} className="badge badge-info" title={el.description}>{el.full_name}</span>)
      i += 1;
    });
    return (<span>{tmpRet}</span>);
  }

  renderOperationsCell(props){
    const { selectedRecord } = this.state;
    const tmpData  = props.original;
    let tmpRet = [];
    if(selectedRecord !==undefined && selectedRecord!==null && selectedRecord.index === props.index){
      tmpRet.push(<span key={0}>
        <button type="button" key={0} className="pure-button pure-button-primary" onClick={this.handleRecordSaveEdit}>Save</button>&nbsp;
        <button type="button" key={1} className="pure-button" onClick={this.handleRecordCancelEdit}>Cancel</button>
      </span>);
    } else {
      if( tmpData.concept_type == 'I' ) {

        if( tmpData.is_visible == '1' ) {
          tmpRet.push(<a key={3} href="#" className="icon01 hide-record" title="hide" onClick={this.handleHideRecord(props)}><img src={ASSETS_PATH + '/icons/eye-show.svg'} /></a>);
        } else {
          tmpRet.push(<a key={4} href="#" className="icon01 show-record" title="show" onClick={this.handleShowRecord(props)}><img src={ASSETS_PATH + '/icons/eye-hide.svg'} /></a>);
        }

      } else if( tmpData.concept_type == 'C' ) {
        tmpRet.push(<a key={1} href="#" className="icon01 edit-record" title="edit this record" onClick={this.handleEditRecordClick(props)}><img src={ASSETS_PATH + '/icons/edit.svg'} /></a>);
        tmpRet.push(<a key={2} href="#" className="icon01 delete-record" title="delete this record" onClick={this.handleDeleteRecordClick(props)}><img src={ASSETS_PATH + '/icons/cancel.svg'} /></a>);
      }
    }
    return (<span>{tmpRet}</span>);
  };

  handleRecordSaveEdit(e) {
    const { selectedRecord } = this.state;
    const CancelToken = axios.CancelToken;

    axios.request({
      url: ajaxurl,
      method: 'post',
      params: {
        action: 'nrb_wp_begrepp_edit_record',
        id: selectedRecord.original.id,
        uuid: selectedRecord.original.uuid,
        term: selectedRecord.original.term,
        msource: selectedRecord.original.msource,
        definition: selectedRecord.original.definition,
        comment: selectedRecord.original.comment
      },
      cancelToken: new CancelToken((c)=>{
        // An executor function receives a cancel function as a parameter
        this.cancel = c;
      })
    })
    .then((response) => {
      if(response && (response.status == 200) && response.data && (response.data.status==='DONE') ){
        //update table data to apply the change
        const row = {...selectedRecord.original};
        let tableData =[
          ...this.state.tableData.slice(0,selectedRecord.index),
          row,
          ...this.state.tableData.slice(selectedRecord.index+1)
        ];

        this.setState({
          tableData,
          tableProps: {
            ...this.state.tableProps,
            data: tableData
          },
          selectedRecord: null
        }, ()=>{
          this.setState({
            content: this.renderTable()
          })
        });
      }else{
        alert('error editing record');
      }
    })
    .catch((error)=>{
      alert('error editing record');

      if (axios.isCancel(error)) {
      } else {
      }

    })
    .then(()=>{
      if(this.cancel) {
        this.cancel();
      }
    })

  }

  handleRecordCancelEdit(e){
    this.setState({
      selectedRecord: null
    },()=>{
      this.setState({
        content: this.renderTable()
      })
    })
  }

  handleHideRecord(props){
    return (e)=>{
      e.preventDefault();
      this.changeRecordVisibilityStatus(props, 0)
    }
  }

  handleShowRecord(props){
    return (e)=>{
      e.preventDefault();
      this.changeRecordVisibilityStatus(props, 1)
    }
  }

  changeRecordVisibilityStatus(data, visibilityStatus){
    const CancelToken = axios.CancelToken;
    axios.request({
      url: ajaxurl,
      method: 'post',
      params: {
        action: 'nrb_wp_begrepp_change_word_visibility',
        id: data.original.id,
        uuid: data.original.uuid,
        is_visible: ''+visibilityStatus
      },
      cancelToken: new CancelToken((c)=>{
        // An executor function receives a cancel function as a parameter
        this.cancel2 = c;
      })
    })
    .then((response)=>{
      if(response && (response.status == 200) && response.data && (response.data.status==='DONE') ){
        const row = {...data.original,
          is_visible: visibilityStatus
        };
        let tableData=this.state.tableData;
        //update table data to apply the change
        tableData=[
          ...tableData.slice(0,data.index),
          row,
          ...tableData.slice(data.index+1)
        ];
        this.setState({
          tableData,
          tableProps: {
            ...this.state.tableProps,
            data: tableData
          }
        },()=>{
          this.setState({
            content: this.renderTable()
          });
        });
//                  alert('change saved');
      }else{
        alert('error changing record visibility status');
      }
    })
    .catch((error)=>{
      alert('error changing record visibility status');

      if (axios.isCancel(error)) {

      } else {

        // handle error
      }

    })
    .then(()=>{
      if(this.cancel2){
        this.cancel2();
      }
    })
  }

  handleEditRecordClick(props){
    return (e)=>{
      e.preventDefault();
      this.setState({
        selectedRecord: {
          index: props.index,
          original: {...props.original},
          value: props.value
        }
      }, ()=>{
        this.setState({
          content: this.renderTable()
        })
      })
    }
  }

  handleDeleteRecordClick(props){
    return (e)=>{
      e.preventDefault();
      const answ = confirm('delete record?');
      if(answ){
        const CancelToken = axios.CancelToken;

        axios.request({
          url: ajaxurl,
          method: 'post',
          params: {
            action: 'nrb_wp_begrepp_delete_record',
            id: props.original.id,
            uuid: props.original.uuid,
          },
          cancelToken: new CancelToken((c)=>{
            // An executor function receives a cancel function as a parameter
            this.cancel3 = c;
          })
        })
        .then((response) => {
          if(response && (response.status == 200) && response.data && (response.data.status==='DONE') ){
            //update table data to apply the change
            let tableData =[
              ...this.state.tableData.slice(0,props.index),
              ...this.state.tableData.slice(props.index+1)
            ];

            this.setState({
              tableData,
              tableProps: {
                ...this.state.tableProps,
                data: tableData
              },
              selectedRecord: null
            }, ()=>{
              this.setState({
                content: this.renderTable()
              })
            });
          }else{
            alert('error deleting record');
          }
        })
        .catch((error)=>{
          alert('error deleting record');

          if (axios.isCancel(error)) {
          } else {
          }

        })
        .then(()=>{
          if(this.cancel3){
            this.cancel3();
          }
        })

      }
    }
  }

  componentWillUnMount(){
    if(this.cancel && typeof this.cancel == 'function'){
      this.cancel();
    }
    if(this.cancel2 && typeof this.cancel2 == 'function'){
      this.cancel2();
    }
    if(this.cancel3 && typeof this.cancel3 == 'function'){
      this.cancel3();
    }
    if(this.cancel4 && typeof this.cancel4 == 'function'){
      this.cancel4();
    }
    if(this.cancel5 && typeof this.cancel5 == 'function'){
      this.cancel5();
    }
  }

  componentDidMount() {
    const CancelToken = axios.CancelToken;

    this.setState({
      requestProgress: 10,
      requestStatus: PROGRESS_STATUS.STARTED
    })

    axios.get(ajaxurl, {
      params: {
        action: 'nrb_wp_begrepp_fetch_words'
      },
      // `onUploadProgress` allows handling of progress events for uploads
      onUploadProgress: (progressEvent) => { },
      // `onDownloadProgress` allows handling of progress events for downloads
      onDownloadProgress: (progressEvent) => { },
      cancelToken: new CancelToken((c)=>{
        // An executor function receives a cancel function as a parameter
        this.cancel4 = c;
      })
    })
    .then((response)=>{
      this.setState({
        requestProgress: 100,
        requestStatus: PROGRESS_STATUS.FINISHED
      })
      if(response && response.data && response.data.status) {
        const res = response.data;
        if(res.status === 'OK') {
          if(res.data && res.data.length > 0) {
            let tableData = res.data;
            this.setState({
              tableData,
              tableProps:{
                ...this.state.tableProps,
                data: tableData
              }
            }, () => {
              const content = this.renderTable();
              this.setState({
                content
              })
            });
          } else {
            this.setState({
              tableData: [],
              tableProps:{
                ...this.state.tableProps,
                data: []
              }
            }, () => {
              const content = this.renderNoDate();
              this.setState({
                content
              })
            });
          }
        }else{
          this.setState({
            errorNo: '100',
            errorMessage: 'error fetching data from server',
            tableData: [],
            tableProps:{
              ...this.state.tableProps,
              data: []
            }
          }, () => {
            const content = this.renderError();
            this.setState({
              content
            })
          });
        }
      } else {
        this.setState({
          errorNo: '101',
          errorMessage: 'error fetching data from server',
          tableData: [],
          tableProps:{
            ...this.state.tableProps,
            data: []
          }
        },()=>{
          const content = this.renderError();
          this.setState({
            content
          })
        });
      }
    })
    .catch((error)=>{
      alert('error fetching record');

      if (axios.isCancel(error)) {
      } else {
      }

    })
    .then(()=>{
      if(this.cancel4){
        this.cancel4();
      }
    })

  }

  renderTable() {
    const { tableProps } = this.state;
    return (<div>
      <ReactTable
        {...tableProps}
      />
    </div>);
  }

  renderNoDate() {
    return (<div>
      <AlertMessage type="info" >
        No date to show please add some custom data or sync first.
        {' '}<Link to={'/sync-data'}>Sync data</Link>
        {' '}<Link to={'/add-custom-data'}>Add custom data</Link>
      </AlertMessage>
    </div>);
  }

  renderError() {
    const { errorNo, errorMessage } = this.state;
    return(<div>
      <AlertMessage type="error" >
        Error ({errorNo}): {errorMessage}
      </AlertMessage>
    </div>)
  }

  render() {
    const { content, requestProgress, requestStatus } = this.state;
    return (<Fragment>
      <TopNav>
      :: <Link to={'/'}>Dashboard</Link> > <span>browse-data</span>
      </TopNav>
      <AjaxProgressBar progress={requestProgress} status={requestStatus} />
      {content}
    </Fragment>);
  }
}

/*!
 * AddCustomData Component
 **/
const AddCustomData = function() {

  const initialState = {
    formData: {
      term: '',
      msource: '',
      definition: '',
      comment: '',
      is_visible: true
    },
    message: null
  };

  const reducer = (curState, action)=> {
    switch(action.type) {
      case 'UPDATE_TERM':
        return {...curState,
          formData: {...curState.formData,
            term: action.payload.term
          }
        };
        break;
      case 'UPDATE_MSOURCE':
        return {...curState,
          formData: {...curState.formData,
            msource: action.payload.msource
          }
        };
        break;
      case 'UPDATE_DEFINITION':
        return {...curState,
          formData: {...curState.formData,
            definition: action.payload.definition
          }
        };
        break;
      case 'UPDATE_COMMENT':
        return {...curState,
          formData: {...curState.formData,
            comment: action.payload.comment
          }
        };
        break;
      case 'DATA_ADDED_SUCCESSFULLY':
        return {...curState,
          formData: {
            term: '',
            msource: '',
            definition: '',
            is_visible: true,
          },
          message: {
            type: 'success',
            message: 'Data added successfully!'
          }
        };
        break;
      case 'ADD_DATA_FAILED':
        return {...curState,
          message: {
            type: 'error',
            message: action.payload.message
          }
        };
        break;
      case 'REMOVE_MESSAGE':
        return {...curState,
          message: null
        };
        break;
      default:
        return {...curState};
    }
  }

  const [state, dispatch] = useReducer(reducer, initialState);

  const handleFormSubmit = (e) => {
    e.preventDefault();
    const CancelToken = axios.CancelToken;
      let cancel;
      axios.request({
        url: ajaxurl,
        method: 'post',
        params: {
          action: 'nrb_wp_begrepp_add_custom_data',
          term: state.formData.term,
          msource: state.formData.msource,
          definition: state.formData.definition,
          comment: state.formData.comment
        },
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          cancel = c;
        })
      })
      .then(function (response) {
        if(response && (response.status == 200) && response.data && (response.data.status==='DONE') ) {
          dispatch({type: 'DATA_ADDED_SUCCESSFULLY'})
          setTimeout(()=>{
            dispatch({type: 'REMOVE_MESSAGE'});
          },3500)
        }else{
          if(response.data.message){
            dispatch({
              type: 'ADD_DATA_FAILED',
              payload:{
                message: response.data.message
              }
            });
          }else{
            dispatch({
              type: 'ADD_DATA_FAILED',
              payload:{
                message: 'unknown error'
              }
            });
          }
        }
      })
      .catch(function (error) {
        if (axios.isCancel(error)) {
          dispatch({
            type: 'ADD_DATA_FAILED',
            payload:{
              message: 'operation canceled'
            }
          });
        } else {
          dispatch({
            type: 'ADD_DATA_FAILED',
            payload:{
              message: 'error adding custom data'
            }
          });

        }
      })
      .then(function () {
        cancel();
      })
  }

  const handleTermChange = (e) => {
    dispatch({
      type: 'UPDATE_TERM',
      payload: {
        term: e.target.value
      }
    });
  }

  const handleMsourceChange = (e) => {
    dispatch({
      type: 'UPDATE_MSOURCE',
      payload: {
        msource: e.target.value
      }
    });
  }

  const handleDefinitionChange = (e) => {
    dispatch({
      type: 'UPDATE_DEFINITION',
      payload: {
        definition: e.target.value
      }
    });
  }

  const handleCommentChange = (e) => {
    dispatch({
      type: 'UPDATE_COMMENT',
      payload: {
        comment: e.target.value
      }
    });
  }

  const { term, msource, definition, comment } = state.formData;
  const { message } = state;

  return (<Fragment>
  <TopNav>
  : <Link to={'/'}>Dashboard</Link> > <span>Add custom data</span>
  </TopNav>
  <div>
    { message
    ?
      <AlertMessage type={message.type}><b>{message.message}</b> </AlertMessage>
    :
      null
}
    <form action="#" method="post" className="pure-form pure-form-stacked" onSubmit={handleFormSubmit}>
      <fieldset>
        <legend>You can add a custom word definition</legend>


        <div className="pure-g">
          <div className="pure-u-1 pure-u-md-1-3">
            <label htmlFor="term">Term</label>
            <input id="term" className="pure-input-1-2" value={term} type="text" onChange={handleTermChange} placeholder="Term" required="required" />
            <span className="pure-form-message">This is a required field.</span>
          </div>

          <div className="pure-u-1 pure-u-md-1-3">
            <label htmlFor="msource">Source</label>
            <input id="msource" className="pure-input-1-2" value={msource} type="text" onChange={handleMsourceChange} placeholder="Source" required="required" />
            <span className="pure-form-message">This is a required field.</span>
          </div>

          <div className="pure-u-1 pure-u-md-1">
            <label htmlFor="definition">Definition</label>
            <textarea id="definition" className="pure-input-1" value={definition} onChange={handleDefinitionChange} placeholder="definition" required="required"></textarea>
            <span className="pure-form-message">This is a required field.</span>
          </div>

          <div className="pure-u-1 pure-u-md-1">
            <label htmlFor="comment">Comment</label>
            <textarea id="comment" className="pure-input-1" value={comment} onChange={handleCommentChange} placeholder="comment"></textarea>
          </div>

        </div>

        <br />
        <button type="submit" class="button button-primary">Add Custom Data</button>
      </fieldset>
    </form>
  </div>
  </Fragment>)
}/**/

/*!
 * TopNav Component
 **/
const TopNav=function(props) {

  const appContext = useContext(AppContext);
  const [el] = useState(document.createElement('div'));

  useEffect(()=>{
    appContext.topNavRoot.appendChild(el);

    return ()=>{
      appContext.topNavRoot.removeChild(el);
    }
  },[])

  return ReactDOM.createPortal(
    props.children,
    el,
  );

}/**/

/*!
 * AlertMessage Component
 **/
const AlertMessage = function(props){

  const {children,type,isDismissible} = props;

  const handleClose=()=>{
  }

  const className = 'settings-error notice' + (type!==undefined?' type-'+type:' ') + (isDismissible!==undefined?' '+isDismissible:' ');

  return (<div id="setting-error-wporg_message" className={className}>
    <p>
      {children}
    </p>
    {
      isDismissible===true?
        (<button type="button" className="notice-dismiss" onClick={handleClose} >
           <span className="screen-reader-text">Dismiss this notice.</span>
         </button>)
      :
        null
    }
  </div>);
}/**/

/*!
 * PROGRESS_STATUS
 **/
const PROGRESS_STATUS={
  NOT_STARTED: -1,
  STARTED: 1,
  FINISHED: 2,
  FAILED:0
};/**/

/*!
 * AjaxProgressBar Component
 **/
const AjaxProgressBar = function(props){
  const appContext = useContext(AppContext);
  const [progress, setProgress] = useState(10+props.progress);
  const [className, setClassName] = useState('progress');
  const [el] = useState(document.createElement('div'));
  const {status} = props;

  useEffect(()=>{
    el.setAttribute('class', 'progress-bar');
    appContext.progressBarRoot.appendChild(el);
    return ()=>{
      appContext.progressBarRoot.removeChild(el);
    }
  },[])

  useEffect(()=>{
    setProgress(props.progress)
  }, [props.progress])

  useEffect(()=>{

    switch(status){
      case PROGRESS_STATUS.FAILED:
        setClassName('progress failed-progress');
        setTimeout(()=>{
          setClassName('progress not-started-progress');
          setProgress(0);
        },2500);
        break;
      case PROGRESS_STATUS.STARTED:
        setClassName('progress');
        break;
      case PROGRESS_STATUS.FINISHED:
        setTimeout(()=>{
          setClassName('progress not-started-progress');
          setProgress(0);
        },500);
        break;
      case PROGRESS_STATUS.NOT_STARTED:
        setClassName('progress not-started-progress');
        break;
      default:
        console.log('unknown progress status, possible bug or crash',status);
    }
  }, [status])

  return ReactDOM.createPortal(
    (<div className={className} style={{width:progress+'%'}}></div>),
    el,
  );

}

/* Render React to DOM */
ReactDOM.render(
  <App />,
  appRoot
);/**/
})()
