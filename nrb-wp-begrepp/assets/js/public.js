
(function($) {
    const urlParams = new URLSearchParams(window.location.search);
    const slug = urlParams.get('slug');
    if(slug) isolateCard(slug);

    // init search when site loads
    if( $('input#tags_search').val() !== '' ) {
        filterCards($('input#tags_search').val());
    }

    $('input#tags_search').on('change paste keyup', debounce(function() {
        filterCards($(this).val());
    }, 500)); 

    function filterCards(val) {
        if(val) {
            history.pushState('', '', '?tags_search=' + val);
        }
        if(val.length < 3) {
            $('.js-tag-card').show();
            history.pushState('','','?tags_search=');
            return false;
        }

        $('.js-tag-card').hide();
        var matcher = new RegExp(val, 'gi');
        $('.js-tag-card')
            .show()
            .not(function () {
                return matcher.test($(this).find('h2, h5').text())
            })
            .hide();
    }

    $('.js-filter-card').click(function() {
        var slug = $(this).data('filter-card');
        isolateCard(slug);
    });

    function isolateCard(slug) {
        $('.js-tag-card').hide();
        $('.js-tag-card#tag_' + slug).show();
        history.pushState('', '', '?slug=' + slug);
    }

    function debounce(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };

}(jQuery));
